\documentclass[]{beamer}

\usetheme{default}

\usepackage{graphicx,amsmath,tabularx}

\graphicspath{{images/}}

\title{Photon Bose-Einstein condensation polarisation}
\author{Ryan Moodie\\Supervisor: Jonathan Keeling}
\institute{School of Physics and Astronomy\\University of St Andrews\\
	\vspace{3ex}\includegraphics[height=0.3\textheight]{st_andrews_logo}}
\date{Summer 2016}

\begin{document}

	\begin{frame}
		\titlepage
	\end{frame}

	% \begin{frame}<beamer>{Outline}
	% 	\tableofcontents[currentsection,currentsubsection]
	% \end{frame}
    
    \begin{frame}{Motivation}
    	\begin{columns}
    		\column{0.25\textwidth}
    			\centering
	    		\includegraphics[height=0.8\textheight]{nature_photon_bec}
    		\column{0.75\textwidth}
    			\begin{itemize}
	        		\item[]{BEC: an exotic state of matter in which a gas of bosons all occupy a single state, displaying quantum phenomena at macroscopic scales.}
	        		\item[]{It was recently achieved for the first time with photons, in a laser-pumped dye-filled microcavity at room temperature (shown to left) \cite{klaers10}.}
	        		\item[]{Experiments at Imperial following \cite{marelic14} indicate that the way light is polarised changes on condensation. While a photon condensation model exists \cite{kirton13}\cite{kirton15}\cite{keeling16}, there is no previous work regarding its polarisation.}
	        		\item[]{To describe this phenomenon, the theory was developed to include polarisation.}
	        	\end{itemize}
        \end{columns}
    \end{frame}

    \begin{frame}{Laser-pumped dye-filled optical microcavity}
    	Schematic diagram (from \cite{kirton15}):        
        \begin{center}
            \includegraphics[width=0.7\textwidth]{diagram2}     
        \end{center}
        \begin{tabularx}{\textwidth}{l l l}
            $\kappa$ & cavity photon loss \\
            $\Gamma(\delta_{a})$ & molecular absorption of photons \\
            $\Gamma(-\delta_{a})$ & molecular emission of photons \\
            $\Gamma_{\uparrow}$ & pumping \\
            $\Gamma_{\downarrow}$ & fluorescence \\
        \end{tabularx}
    \end{frame}

    \begin{frame}{Original equations}
        The rate equations without polarisation are:
        \begin{equation}
            \frac{\partial}{\partial t} \, n_{a} = - \kappa \, n_{a} + \Gamma(-\delta_{a}) \, \Big( n_{a} + 1 \Big) \, N_{\uparrow} 
             - \Gamma(\delta_{a}) \, n_{a} \, \Big( N - N_{\uparrow} \Big)
            \label{eq:photonOriginal}
        \end{equation}
        
        \begin{multline}
            \frac{\partial}{\partial t} \, N_{\uparrow} = 
            - \, \Gamma_{\downarrow} \, N_{\uparrow} \, + \, \Gamma_{\uparrow} \Big( N - N_{\uparrow} \Big)
            + \, \sum_{a=0}^{\infty} g_{a} \bigg \{ \\ \Gamma(\delta_a) \, n_a \, \Big( N - N_{\uparrow} \Big) \, - \, \Gamma(-\delta_a) \Big( n_{a} + 1 \Big) \, N_{\uparrow} \, \bigg \}
            \label{eq:moleculeOriginal}
        \end{multline}
        
        \begin{tabularx}{\textwidth}{l l l}
            $n_{a}$ & $a^{th}$ photon mode population \\
            $N_{\uparrow}$ & molecular excited state population \\
            $g_{a}$ & degeneracy factor ($ = a + 1$) \\
            $N$ & total number of molecules \\
        \end{tabularx}
    \end{frame}

    \begin{frame}{Polarisation extension}
        The equations were recast to include the angular dependence of the molecule-photon interaction: \\
        \begin{itemize}
            \item{$n_{a} \rightarrow n_{a}^{\sigma}$}
            \item{$N_{\uparrow} \rightarrow N_{\uparrow}(\theta, \phi)$}
            \item{introduce molecular angular diffusion term}
            \item $N_{\uparrow}(\theta, \phi) \rightarrow 2 \sqrt{\pi} \sum_{l, m} N_{l, m} Y_{l, m}(\theta, \phi)$.
        \end{itemize}
        with: \\
        \begin{tabularx}{\textwidth}{l l l l}
            $\sigma$ & polarisation state \\
            $D$ & diffusion coefficient \\
            $\theta$ & polar angle \\
            $\phi$ & azimuthal angle \\
            $\chi$ & pump polarisation angle \\
            $Y_{l, m}(\theta, \phi)$ & spherical harmonics
        \end{tabularx}
    \end{frame}

    % \begin{frame}{Equations with polarisation}
    %     \begin{equation}
    %         \beta^{\sigma}(\varphi) = 
    %         \begin{cases}
    %             \cos^{2}(\varphi) & \text{for } \sigma = x \\
    %             \sin^{2}(\varphi) & \text{for } \sigma = y
    %         \end{cases}
    %         \label{eq:beta}
    %     \end{equation}
        % \begin{multline}
    %         \frac{\partial}{\partial t} \, n_{a}^{\sigma} = - \kappa \, n_{a}^{\sigma} 
    %         + ( n_{a}^{\sigma} + 1 ) \, \Gamma(-\delta_{a}) \int_{0}^{\pi} d \theta \, \sin(\theta) \int_{0}^{2 \pi} d \phi \, \sin^{2}(\theta) \, \beta^{\sigma}(\phi) \, N_{\uparrow}(\theta, \phi) \\
    %          - n_{a}^{\sigma} \, \Gamma(\delta_{a}) \int_{0}^{\pi} d \theta \, \sin(\theta) \int_{0}^{2 \pi} d \phi \, \sin^{2}(\theta) \, \beta^{\sigma}(\phi) \, \Big( N - N_{\uparrow}(\theta, \phi) \Big)
    %          \label{eq:photonAngle}
    %     \end{multline}
    %     \begin{multline}
    %         \frac{\partial}{\partial t} \, N_{\uparrow}(\theta, \phi) = \,
    %         - \, \Gamma_{\downarrow} \, N_{\uparrow}(\theta, \phi) \,
    %         + \, \sum_{\sigma=(x, y)} \Bigg\{ \, \beta^{\sigma}(\chi) \, \Gamma_{\uparrow} \, \sin^{2}(\theta) \, \beta^{\sigma}(\phi) \, \Big( N - N_{\uparrow}(\theta, \phi) \Big) \\ 
    %         - \, \sum_{a=0}^{\infty} \, g_{a} \Bigg[ \Gamma(-\delta_{a}) \, ( n_{a}^{\sigma} + 1 ) \, \sin^{2}(\theta) \, \beta^{\sigma}(\phi) \, N_{\uparrow}(\theta, \phi) \,
    %         + \, \Gamma(\delta_{a}) \, n_{a}^{\sigma} \, \sin^{2}(\theta) \, \beta^{\sigma}(\phi) \, \Big( N - N_{\uparrow}(\theta, \phi) \Big) \Bigg] \Bigg\} \\ 
    %         + \, D \, \Bigg[ \frac{1}{\sin^{2}(\theta)} \, \frac{\partial^{2}}{\partial \phi^{2}} + \frac{1}{\sin(\theta)} \, \frac{\partial}{\partial \theta} \bigg( \sin(\theta) \, \frac{\partial}{\partial \theta} \bigg) \Bigg] \, N_{\uparrow}(\theta, \phi)
    %         \label{eq:moleculeAngle}
    %     \end{multline}
    % \end{frame}

    \begin{frame}{Solving the equations}
        A Python program was written to solve the rate equations:
        
        \begin{itemize}
        \item Parameters were set as in \cite{keeling16}.
        \item $D$ was set matching experimental data \cite{bojarski96}.
        \item To test the simulation, steady-state spectra were plotted and found to match expectations:
        \end{itemize}
        
        \begin{center}
        \includegraphics[width=0.75\textwidth]{report_population_distribution_0}
        \end{center}
        
    \end{frame}

    \begin{frame}{Population behaviour}
        \begin{center}
            \begin{tabularx}{\textwidth}{l l}
                \hspace{-2.75ex} \includegraphics[width=0.5\textwidth]{report_pumping_response_photon_mode_0_crop}
                &
                \hspace{-2.75ex} \includegraphics[width=0.5\textwidth]{report_pumping_response_excited_state_1_crop} \\
                \begin{minipage}[t]{0.45\textwidth}
                    \begin{flushleft}
                        A clear threshold is seen with exotic multiple mode condensation.
                    \end{flushleft}
                \end{minipage}
                &
                \begin{minipage}[t]{0.45\textwidth}
                    \begin{flushleft}
                        Above threshold, $N_{0, 0}$ (related to $N_{\uparrow}$) clamps and higher orders (reflecting molecule orientation distribution) suppress.
                    \end{flushleft}
                \end{minipage} \\
            \end{tabularx}
        \end{center}
        
    \end{frame}

    \begin{frame}{Polarisation response}
        % \begin{flushleft}
        %     Polarisation degree:
        % \end{flushleft}
		% \centering
        % \begin{tabularx}{\textwidth}{l l l}
        %     pump & : & $P_{in}$ = $\cos(2 \chi)$ \\
        %     cavity photons & : & $P_{out}$ = $\frac{x-y}{x+y}$ \\
        % \end{tabularx}

        \begin{columns}        
	        \column{0.4\textwidth}
		        \begin{flushleft}
		            With respect to threshold,
		        \end{flushleft}
		        \begin{itemize}
		            \item{Below: Weakly polarised photon gas; spontaneous emission allows molecule rotation time.}
		            \item{Just above: Strongly polarised condensate as single condensation mode.}
		            \item{Far above: $P_{out}$ follows $P_{in}$; stimulated emission suppresses diffusion.}
		        \end{itemize}
	        \column{0.7\textwidth}
	            \includegraphics[width=\textwidth]{report_2d_colour_map_0}
        \end{columns}
        
    \end{frame}

    \begin{frame}{Summary}
    	\begin{itemize}
        \item{An existing theoretical model of photon BEC in a dye-filled microcavity was extended to include polarisation.}
        \item{A computational simulation of the system was produced to investigate its behaviour.}
        \item{The model generally predicts expected results with some interesting exotic behaviour, furthering our understanding of photon condensation.}
        \item{Some unusual outcomes have motivated a project extension.}
        \end{itemize}
    \end{frame}

    \begin{frame}{References}
        \bibliographystyle{plain}
        \bibliography{references}
    \end{frame}

    \begin{frame}{Advice}
    	Talk to people!
    \end{frame}

    \begin{frame}{Future outlook}
        \begin{center}
        \includegraphics[width=0.75\textwidth]{report_output_polarisation_degree_against_diffusion_0}
        \end{center}
        
        A project extension will be undertaken to investigate some curious results, such as $P_{out}$ remaining small for a fully polarised pump when molecular angular diffusion is suppressed.             
    \end{frame}

\end{document}
